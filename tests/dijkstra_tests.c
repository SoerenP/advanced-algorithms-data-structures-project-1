#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <AdvAlgDat/minunit.h>
#include <AdvAlgDat/Heap.h>
#include <AdvAlgDat/Dijkstra.h>
#include <AdvAlgDat/graph_util.h>

#define MAXSIZE 256

Heap *H = NULL;

int check_connectivity(Graph *G)
{
  // BFS through the graph and count how many new nodes are encountered. If the number of nodes encountered equals the size of the graph, the graph is fully connected.
  int visited[G->n];
  int stack[G->n];
  int i;
  for (i = 0; i < G->n; i++)
    {
      visited[i] = 0;
    }
  stack[0] = 0;
  int stack_size = 1;
  visited[0] = 1;
  int number_visited = 1;
  while (stack_size > 0)
    {
      int node = stack[--stack_size];
      
      
      for (i = 0; i < G->n; i++)
	{
	  if (G->weights[node][i] != NO_EDGE && !visited[i])
	    {
	      stack[stack_size++] = i;
	      visited[i] = 1;
	      number_visited++;
	    }
	}
    }

  if (number_visited == G->n) return 1;
  else return 0;

  
}

char *test_shortest_path_bin_heap()
{
  Graph *G = malloc(sizeof(*G));
  G->n = 6;
  float weights[6][6] = {{ 0.0, 2.0,-1.0,-1.0, 3.0,-1.0},
		 	 {-1.0, 0.0, 5.0,-1.0,-1.0, 1.0},
			 { 2.0, 1.0, 0.0,-1.0,-1.0,-1.0},
			 { 1.0, 1.0, 2.0, 0.0, 1.0,-1.0},
			 {-1.0,-1.0, 4.0, 2.0, 0.0, 1.0},
			 { 4.0,-1.0, 1.0, 5.0,-1.0, 0.0}};

  G->weights = malloc(G->n * sizeof(float*));
  int i,j;
  for (i = 0; i < G->n; i++)
    {
      G->weights[i] = malloc(G->n * sizeof(float));
      for (j = 0; j < G->n; j++)
	{
	  G->weights[i][j] = weights[i][j];
	}
    }
  
  Path *p = DijkstraBinHeap(G, 0);
  
  mu_assert(p->dist[3] == 5.0, "Wrong length of path calculated!");
  mu_assert(p->prev[3] == 4 && p->prev[4] == 0, "Wrong path chosen!");
  DestroyGraph(G);
  DestroyPath(p);
  return NULL;
}

char *test_shortest_path_fib_heap()
{
  Graph *G = malloc(sizeof(*G));
  G->n = 6;
  float weights[6][6] = {{ 0.0, 2.0,-1.0,-1.0, 3.0,-1.0},
		 	 {-1.0, 0.0, 5.0,-1.0,-1.0, 1.0},
			 { 2.0, 1.0, 0.0,-1.0,-1.0,-1.0},
			 { 1.0, 1.0, 2.0, 0.0, 1.0,-1.0},
			 {-1.0,-1.0, 4.0, 2.0, 0.0, 1.0},
			 { 4.0,-1.0, 1.0, 5.0,-1.0, 0.0}};

  G->weights = malloc(G->n * sizeof(float*));
  int i,j;
  for (i = 0; i < G->n; i++)
  {
      G->weights[i] = malloc(G->n * sizeof(float));
      for (j = 0; j < G->n; j++)
	{
	  G->weights[i][j] = weights[i][j];
	}
    }
  
  Path *p = DijkstraFibHeap(G, 0);

  mu_assert(p->dist[3] == 5.0, "Wrong length of path calculated!");
  mu_assert(p->prev[3] == 4 && p->prev[4] == 0, "Wrong path chosen!");
  DestroyGraph(G);
  DestroyPath(p);
  return NULL;
}


char *test_same_result()
{
  int n = 10000;
  Graph *G = MakeRandomGraph(n,20.0/(float)n);
  Path *fibP = DijkstraFibHeap(G, 0);
  Path *binP = DijkstraBinHeap(G, 0);

  int i;
  for (i = 1; i < binP->n; i++)
    {
      //printf("%d'th iteration:\n", i);
      //printf("Comparing %f to %f\n", fibP->dist[i], binP->dist[i]);
      mu_assert(fibP->dist[i] == binP->dist[i], "Algorithms returned different distances!");
      //printf("Comparing %d to %d\n", fibP->prev[i], binP->prev[i]);
      mu_assert(fibP->prev[i] == binP->prev[i], "Algorithms returned different paths!");
    }

  DestroyGraph(G);
  DestroyPath(fibP);
  DestroyPath(binP);

  return NULL;
}

char *test_sparse_graph_connectivity()
{
  //printf("------------- SPARSE GRAPH: -------------------\n");
  int n = 100;
  Graph *G = MakeRandomGraph(n,0.0);
  mu_assert(check_connectivity(G), "Graph was not fully connected!");
  
  
  return NULL;
}

char *test_dense_graph_connectivity()
{
  //printf("------------- DENSE GRAPH: -------------------\n");
  int n = 100;
  Graph *G = MakeRandomGraph(n,1.0);
  mu_assert(check_connectivity(G), "Graph was not fully connected!");
  
  
  return NULL;
}





/* this runs all tests in order */
char *all_tests()
{
  srand(time(NULL));
  mu_suite_start();

	
  mu_run_test(test_shortest_path_bin_heap);
  mu_run_test(test_shortest_path_fib_heap);
  mu_run_test(test_same_result);
  mu_run_test(test_sparse_graph_connectivity);
  mu_run_test(test_dense_graph_connectivity);
	
	
  return NULL;
}

/* this macro expands to a main function */
RUN_TESTS(all_tests);
