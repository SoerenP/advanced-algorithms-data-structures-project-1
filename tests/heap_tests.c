#include <stdio.h>
#include <stdlib.h>
#include <AdvAlgDat/minunit.h>
#include <AdvAlgDat/Heap.h>
#include <time.h>
#define MAXSIZE 256

Heap *H = NULL;

char *test_heap_destroy()
{
	DestroyHeap(H);

	return NULL;
}

char *test_heap_construct()
{
  int n = 5;
  float data[5] = {4.0, 5.0, 3.0, 1.0, 2.0};
  Vertex V[5];
  int i;
  for (i = 0; i < n; i++)
    {
      V[i].v = i;
      V[i].k = data[i];
    }

  H = MakeHeap(V,n,n);
  
  mu_assert(H != NULL, "H failed");

  DestroyHeap(H);
  return NULL;
}

char *test_heap_order()
{

  int n = 10;
  float data[10] = {3.4, 6.2, 1.1, 2.2, 5.9, 4.3, 2.0, 7.2, 9.8, 8.1};

  Vertex V[10];
  int i;
  for (i = 0; i < n; i++)
    {
      V[i].v = i;
      V[i].k = data[i];
    }

  H = MakeHeap(V,n,n);
  
  int j;
  for (j = 2; j<=n; j++){
    int i = j/2;
    mu_assert(H->A[i-1].k <= H->A[j-1].k, "Heap order was not preserved!");
  }

  free(H);
  return NULL;
}

char *test_heap_find_min()
{

  int n = 10;
  float data[10] = {3.4, 6.2, 1.1, 2.2, 5.9, 4.3, 2.0, 7.2, 9.8, 8.1};

  Vertex V[10];
  int i;
  for (i = 0; i < n; i++)
    {
      V[i].v = i;
      V[i].k = data[i];
    }

  H = MakeHeap(V,n,n);
  
  /*int j;
  for (j = 0; j < n; j++)
    {
      printf("%f, ", H->A[j]);
    }
    printf("\n%f",FindMin(H));*/
  float expected = 1.1;
  mu_assert(expected == FindMin(H).k, "Minimum value was wrong!");
  mu_assert(n = H->n, "Minimum value reduced the size of the heap!");

  free(H);
  return NULL;
}

char *test_heap_delete_min()
{

  int n = 10;
  float data[10] = {3.4, 6.2, 1.1, 2.2, 5.9, 4.3, 2.0, 7.2, 9.8, 8.1};

  Vertex V[10];
  int i;
  for (i = 0; i < n; i++)
    {
      V[i].v = i;
      V[i].k = data[i];
    }

  H = MakeHeap(V,n,n);

  float expected = 1.1;
  mu_assert(expected == DeleteMin(H).k, "Minimum value was wrong!");
  mu_assert(n = H->n-1, "DeleteMin did not reduce the size of the heap!");
  expected = 2.0;
  mu_assert(expected == FindMin(H).k, "Next minimum value is wrong!");

  free(H);
  return NULL;
}

char *test_heap_insert()
{

  int n = 10;
  float data[10] = {3.4, 6.2, 1.1, 2.2, 5.9, 4.3, 2.0, 7.2, 9.8, 8.1};

  Vertex V[10];

  H = MakeHeap(V,0,n);
  
  int j;
  for (j = 0; j < n; j++)
    {
      Insert(H,j,data[j]);
    }
  float expectedMin = 1.1;
  mu_assert(expectedMin == FindMin(H).k, "Minimum value was wrong!");
  mu_assert(n = H->n, "DeleteMin did not reduce the size of the heap!");

  free(H);
  return NULL;
}

char *test_heap_sort()
{
  int n = 10;
  float data[10] = {4.0, 7.0, 5.0, 3.0, 1.0, 2.0, 9.0, 6.0, 8.0, 10.0};

  Vertex V[10];
  int i;
  for (i = 0; i < n; i++)
    {
      V[i].v = i;
      V[i].k = data[i];
    }

  H = MakeHeap(V,n,n);

  Vertex sortedData[10];

  for (i = 0; i < n; i++)
    {
      sortedData[i] = DeleteMin(H);
    }

  for (i = 1; i < n; i++)
    {
      mu_assert(sortedData[i-1].k <= sortedData[i].k, "Data was not in sorted order!");
    }
  
  free(H);
  return NULL;
}

char *test_heap_decrease_key()
{

  int n = 10;
  float data[10] = {4.0, 7.0, 5.0, 3.0, 1.0, 2.0, 9.0, 6.0, 8.0, 10.0};

  Vertex V[10];
  int i;
  for (i = 0; i < n; i++)
    {
      V[i].v = i;
      V[i].k = data[i];
    }

  H = MakeHeap(V,n,n);

  DecreaseKey(H,7,0.5);
  /*int j;
  for (j = 0; j < n; j++)
    {
      printf("%f, ", H->A[j].k);
    }
    printf("\n%f",FindMin(H).k);
  */
  mu_assert(0.5 == FindMin(H).k, "Key was not decreased properly!");

  DestroyHeap(H);
  return NULL;
}

char *test_heap_decrease_key_a_lot()
{
  int n = 1000;

  Vertex *V = malloc(n * sizeof(*V));
  int i;
  for (i = 0; i < n; i++)
    {
      V[i].v = i;
      V[i].k = rand();
    }

  H = MakeHeap(V,n,n);

  int v;
  for (v = 0; v < n; i++)
    {
      //Vertex decreased = DecreaseKey(H,Dict,v,-1.0);
      //mu_assert(decreased.v == v && decreased.k == -1.0, "Wrong key returned!");
      //ExtractMin(H);
    }

  DestroyHeap(H);

  return NULL;
}


/* this runs all tests in order */
char *all_tests()
{
  srand(time(NULL));
	mu_suite_start();

	
	mu_run_test(test_heap_construct);
	mu_run_test(test_heap_order);
	mu_run_test(test_heap_find_min);
	mu_run_test(test_heap_delete_min);
	mu_run_test(test_heap_insert);
	mu_run_test(test_heap_sort);
	mu_run_test(test_heap_decrease_key);
	
	return NULL;
}

/* this macro expands to a main function */
RUN_TESTS(all_tests);
