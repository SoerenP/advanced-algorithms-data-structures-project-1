#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <AdvAlgDat/minunit.h>
#include <AdvAlgDat/dbg.h>
#include <AdvAlgDat/fheap.h>
#include <AdvAlgDat/Vertex.h>

#define TEST_SIZE 10

static fheap *fh = NULL;
/* test_array should be sorted for tests to make sense */
static float test_array[TEST_SIZE] = {2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0};
static node *node_array[TEST_SIZE];


#define FUZZ_SIZE 10000
static float fuzzing_array[FUZZ_SIZE];

#define DEFAULT_V 0

void setup_fuzzing_array()
{
	int index = 0;
	for(index = 0; index < FUZZ_SIZE; index++)
	{
		fuzzing_array[index] = (float) rand();
	}
}

char *test_heap_sort()
{
	fh = fheap_create(FUZZ_SIZE);
	mu_assert(fh != NULL, "Failed to create fheap.");

	float sorted_data[FUZZ_SIZE];

	/* insert */
	int index = 0;
	for(index = 0; index < FUZZ_SIZE; index++)
	{
		fheap_insert(fh,MakeVertex(DEFAULT_V,fuzzing_array[index]));
	}

	/* delete min and take its key into sorted data */
	for(index = 0; index < FUZZ_SIZE; index++)
	{
		node *temp = fheap_extract_min(fh);
		sorted_data[index] = GET_KEY(temp);
	}

	/* check if sorted */
	for(index = 1; index < FUZZ_SIZE; index++)
	{
		mu_assert(sorted_data[index-1] <= sorted_data[index],
			"Data was not in sorted order."); 
	}

	fheap_destroy(fh);

	return NULL;
}

char *test_fuzzing()
{
	fh = fheap_create(FUZZ_SIZE);

	int index = 0;
	node *temp = NULL;
	for(index = 0; index < FUZZ_SIZE; index++)
	{
		temp = fheap_insert(fh,MakeVertex(DEFAULT_V,fuzzing_array[index]));
		mu_assert(temp != NULL, "Failed to insert into fheap.");
	}

	//grab last note, decrease its key, see if we can find it.
	float new_key = -1.0;
	int rc = fheap_decrease_key(fh,temp,new_key);
	mu_assert(rc != FHEAP_ERR, "Failed to decrease key");
	mu_assert(GET_KEY(temp) == new_key, 
		"Failed to set key to new_key.");

	temp = fheap_find_min(fh);
	mu_assert(GET_KEY(temp) == new_key, "The new min should be the decreased key.");


	fheap_destroy(fh);

	return NULL;
}

char *test_insert()
{
	fh = fheap_create(TEST_SIZE);
	mu_assert(fh != NULL, "Failed to create FHeap.");

	int index = 0;
	node *temp = NULL;
	for(index = 0; index < TEST_SIZE; index++)
	{
		temp = fheap_insert(fh,MakeVertex(DEFAULT_V,test_array[index]));
		mu_assert(temp != NULL, "Failed to insert into FHeap.");
	}

	fheap_destroy(fh);

	return NULL;
}

char *test_decrease_key()
{
	/* create and insert */
	
	fh = fheap_create(TEST_SIZE);
	mu_assert(fh != NULL, "Failed to create FHeap.");

	int index = 0;
	node *temp = NULL;
	for(index = 0; index < TEST_SIZE; index++)
	{
		temp = fheap_insert(fh,MakeVertex(DEFAULT_V,test_array[index]));
		mu_assert(temp != NULL, "Failed to insert into FHeap.");
		node_array[index] = temp;
	}


	//Decrease the biggest by its amount. Extract min and see if its there? 
	float new_key = 1.0;
	int rc = fheap_decrease_key(fh,node_array[TEST_SIZE-1],new_key);
	mu_assert(rc != FHEAP_ERR, "Failed to decrease key");
	mu_assert(GET_KEY(node_array[TEST_SIZE-1]) == new_key, 
		"Failed to set key to new_key.");

	node *min = fheap_find_min(fh);
	mu_assert(GET_KEY(min) == new_key, "The decreased key should be new min.");


	//Lets try to do it again!
	new_key = 0.0;
	rc = fheap_decrease_key(fh,node_array[TEST_SIZE-2],new_key);
	mu_assert(rc != FHEAP_ERR, "Failed to decrease key.");
	mu_assert(GET_KEY(node_array[TEST_SIZE-2]) == new_key,
		"Failed to set key to new_key.");

	min = fheap_find_min(fh);
	mu_assert(GET_KEY(min) == new_key, "The decreased key should be new min.");

	/* cleanup */
	fheap_destroy(fh);

	return NULL;
}

char *test_decrease_key_alot()
{
	fh = fheap_create(TEST_SIZE);
	mu_assert(fh != NULL, "Failed to create heap.");

	
	int index = 0;
	node *temp = NULL;
	for(index = 0; index < TEST_SIZE; index++)
	{
		temp = fheap_insert(fh,MakeVertex(DEFAULT_V,test_array[index]));
		mu_assert(temp != NULL, "Failed to insert into FHeap.");
		node_array[index] = temp;
	}

	/* decrease key each element in node_array so that it is the new minmun, extract min at see if you got the right one */
	for(index = 0; index < TEST_SIZE; index++)
	{
		/* make sure the new key is the minimum */
		float new_key = GET_KEY(fh->root) - 1.0;
		fheap_decrease_key(fh,node_array[index],new_key);
		node *extract_min = fheap_extract_min(fh);
		mu_assert((extract_min == node_array[index]) && (extract_min != NULL), "The extracted min should be the key we decreased below the root");
		mu_assert(GET_KEY(extract_min) == new_key, "extracted key should be equal to new key");
	}


	fheap_destroy(fh);
	return NULL;
}

char *test_extract_min_find_min()
{
	fh = fheap_create(TEST_SIZE);
	mu_assert(fh != NULL, "Failed to create fheap.");

	/* insert */
	int index = 0;
	node *temp = NULL;
	for(index = 0; index < TEST_SIZE; index++)
	{
		temp = fheap_insert(fh,MakeVertex(DEFAULT_V,test_array[index]));
		mu_assert(temp != NULL, "Failed to insert into FHeap.");
	}
	
	/* extract min and find min tests */
	for(index = 0; index < TEST_SIZE; index++)
	{
		node *found_min = fheap_find_min(fh);
		mu_assert(GET_KEY(found_min) == test_array[index],
			"Found min should be minimal");

		node *extract_min = fheap_extract_min(fh);
		mu_assert(GET_KEY(extract_min) == test_array[index], 
			"Delete_Min should be minimal.");
	}

	fheap_destroy(fh);

	return NULL;
}

char *all_tests()
{
	srand(time(NULL));
	setup_fuzzing_array();

	mu_suite_start();

	mu_run_test(test_insert);

	mu_run_test(test_extract_min_find_min);

	mu_run_test(test_decrease_key);

	mu_run_test(test_fuzzing);

	mu_run_test(test_heap_sort);

	mu_run_test(test_decrease_key_alot);

	return NULL;
}

RUN_TESTS(all_tests);
