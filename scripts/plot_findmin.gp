# Usage:
# 	gnuplot -c plot_findmin.gp filename title ytitle
set title ARG3
set xlabel "N"
set logscale x 2
set ylabel ARG4
plot 	ARG1 using 1:6 with linespoints title "Fibonnaci Heap", \
	ARG2 using 1:6 with linespoints title "Binary Heap"
pause -1 "Hit any key to continue"
