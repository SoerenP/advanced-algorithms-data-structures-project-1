#usage: gather_data_dijkstra.sh datafile iterations
#!/bin/bash
k=$2

echo "Running Experiments"
bash gather_dijkstra.sh $k | tee ../data/$1
