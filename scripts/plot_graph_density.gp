#usage:
# gnuplot -c plot_graph_density datafile title1
#plotting sparse graphs 1:4
set title ARG2
set xlabel "Nodes"
set logscale x 2
set logscale y 2
set ylabel "Edges"
plot	ARG1 using 1:4 with linespoints title "Density Sparse", \
	ARG1 using 1:7 with linespoints title "Density Dense"
pause -1 "Hit any key to continue"
