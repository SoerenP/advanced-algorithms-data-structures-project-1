#Usage:
#	gnuplot -c plot_dijkstra.gp datafile title1 title2
#first plot sparse
set title ARG2
set xlabel "N"
set logscale x 2
set logscale y 2
set ylabel "Time in seconds"
plot	ARG1 using 1:2 with linespoints title "Fibonnaci Heap Dijkstra", \
	ARG1 using 1:3 with linespoints title "Binary Heap Dijkstra"
pause -1 "Hit any key to continue"


#plot dense
set title ARG3
set xlabel "N"
set logscale x 2
set logscale y 2
set ylabel "Time in seconds"
plot	ARG1 using 1:5 with linespoints title "Fibonnaci Heap Dijkstra", \
	ARG1 using 1:6 with linespoints title "Binary Heap Dijkstra"
pause -1 "Hit any key to continue"
