#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <AdvAlgDat/fheap.h>
#include <AdvAlgDat/Vertex.h>

#define DEF_V 0
#define EXPECTED_ARGS 3

double time_findmin_fheap(Vertex *A, int n, int iterations)
{
	clock_t begin, end;
	double time_spent = 0.0;

	fheap *fh = fheap_create(n);
	node *temp = NULL;
	int index = 0;
	for(index = 0; index < n; index++)
	{
		temp = fheap_insert(fh,A[index]);
	}

	/* do work */
	for(index = 0; index < iterations; index++)
	{
		
		/* begin counting */
		begin = clock();
		
		temp = fheap_find_min(fh);

		/* stop counting */
		end = clock();

		time_spent += (double)(end - begin) / CLOCKS_PER_SEC;
	}


	/* O2 plz no remove plz */
	float rc = GET_KEY(temp);
	rc++;

	/* cleanup */
	fheap_destroy(fh);

	return time_spent;
}

double time_make_fheap(Vertex *A, int n)
{
	clock_t begin, end;
	double time_spent = 0.0;
	int index = 0;

	/* begin counting */
	begin = clock();

	/* do work */
	fheap *fh = fheap_create(n);

	for(index = 0; index < n; index++)
	{
		fheap_insert(fh,A[index]);
	}

	/* stop counting */
	end = clock();

	/* cleanup */
	fheap_destroy(fh);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

double time_extract_min_fheap(Vertex *A, int n, int iterations)
{
	clock_t begin, end;
	double time_spent = 0.0;
	int index = 0;

	/* setup stuff we dont want to count */
	fheap *fh = fheap_create(n+iterations);	
	node *temp = NULL;
	for(index = 0; index < n; index++)
	{
		temp = fheap_insert(fh,A[index]);
	}

	/* use temp so O2 doesnt remove call due to unused */
	float key = temp->v.k;
	key++;
	
	int array_index = index;

	/* do work */
	for(index = 0; index < iterations; index++)
	{

		/* start counting */
		begin = clock();

		temp = fheap_extract_min(fh);
		
		/* stop counting */
		end = clock();

		/* make sure still size n */
		fheap_insert(fh,A[array_index+index]);
		
		time_spent += (double)(end - begin) / CLOCKS_PER_SEC;
	}

	
	/* use node so above doesn't disappear due to -O2 */
	key = temp->v.k;
	key++;

	/* cleanup */
	fheap_destroy(fh);

	return time_spent;
}

double time_insert_fheap(Vertex *A, int n, int iterations)
{
	clock_t begin, end;
	double time_spent = 0.0;
	int index = 0;

	/* setup stuff we dont want to count */
	/* we want a heap of size n when inserting */
	fheap *fh = fheap_create(n+iterations);	
	node *temp = NULL;
	
	for(index = 0; index < n; index++)
	{
		temp = fheap_insert(fh,A[index]);
	}

	for(index = 0; index < iterations; index++)
	{
		/* start counting */
		begin = clock();

		temp = fheap_insert(fh,A[index]);

		/* stop counting */
		end = clock();

		/* remove one element so we are still at n */
		temp = fheap_extract_min(fh);

		time_spent += (double)(end - begin) / CLOCKS_PER_SEC;
	}
	

	/* use temp so O2 doesnt remove call due to unused */
	float key = temp->v.k;
	key++;
	/* cleanup */
	fheap_destroy(fh);

	return time_spent;
}

double time_decrease_key_fheap(Vertex *A, int n, int iterations)
{	
	clock_t begin, end;
	double time_spent = 0.0;
	int index = 0;

	/* setup stuff we dont want to count */
	fheap *fh = fheap_create(n+iterations);	

	node *temp = NULL;

	/* instead of this, we could perhaps just take random nodes from */
	/* fh->node_pool ? */
	node **node_array = malloc(sizeof(*node_array) * (n + iterations));
	for(index = 0; index < n+1; index++)
	{
		node_array[index] = fheap_insert(fh,A[index]);
	}

	/* force heap to consolidate */
	temp = fheap_extract_min(fh);
	fprintf(stderr,"%f\n",GET_KEY(temp));

	/* start counting */
	int rc = 0;

	begin = clock();
	for(index = 0; index < iterations; index++)
	{


		rc = fheap_decrease_key(
			fh,
			node_array[index],
			GET_KEY(node_array[index])/2);

		
	}
	
	/* stop counting */
	end = clock();
	
	/* use temp so O2 doesn't remove it */
	rc++;
	
	/* node reff in node_array are taking care of by fheap_destroy */
	if(node_array){
		free(node_array);
	}
	fheap_destroy(fh);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;

}

void setup_vertex_array(Vertex *A, int n)
{
	int index = 0;
	for(index = 0; index < n; index++)
	{
		A[index] = MakeVertex(index, (float) rand());
	}
}

float average(float *A, int size)
{
	int index = 0;
	float sum = 0;
	for(index = 0; index < size; index++)
	{
		sum += A[index];
	}

	return sum / size;
}


int main(int argc, char **argv)
{
	if(argc < EXPECTED_ARGS){
		printf("Usage: %s n (elements) k (iterations)\n",argv[0]);
		exit(1);
	}

	/* setup stuff */
	srand(time(NULL));

	int n = atoi(argv[1]);
	int k = atoi(argv[2]);

	float time_insert[k];
	float time_extractmin[k];
	float time_decrease_key[k];
	float time_fheap_create[k];
	float time_fheap_find_min[k];

	int single_test_iterations = 1000;
	
	printf("#----Timing results for fheap at n = %d, k = %d, iter = %d----\n",n,k,single_test_iterations);
	printf("#n \tmakeheap \tinsert \t\tdeleteMin \tdecreaseKey  \tfindMin\n");


	int iterations = 0;
	for(iterations = 0; iterations < k; iterations++)
	{
						
		Vertex *test_array = calloc(sizeof(Vertex),n+single_test_iterations);
		setup_vertex_array(test_array,n+single_test_iterations);
	
		time_insert[iterations] = time_insert_fheap(test_array,n,single_test_iterations);
		time_extractmin[iterations]  = time_extract_min_fheap(test_array,n,single_test_iterations);
		time_decrease_key[iterations] = time_decrease_key_fheap(test_array, n,single_test_iterations);
		time_fheap_create[iterations] = time_make_fheap(test_array,n);
		time_fheap_find_min[iterations] = time_findmin_fheap(test_array, n,single_test_iterations);
		free(test_array);
	}

	float avrg_insert_time = average(time_insert,k);
	float avrg_extract_min = average(time_extractmin,k);
	float avrg_decrease_key = average(time_decrease_key,k);
	float avrg_fheap_create = average(time_fheap_create,k);
	float avrg_fheap_find_min = average(time_fheap_find_min,k);

	printf("%d \t%f \t%f \t%f \t%f \t%f\n",n,avrg_fheap_create,avrg_insert_time,avrg_extract_min,avrg_decrease_key,avrg_fheap_find_min);

	return 0;
}
