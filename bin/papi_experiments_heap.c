#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <papi.h>

#include <AdvAlgDat/Heap.h>
#include <AdvAlgDat/papi_wrap.h>

#define DEF_V 0
#define EXPECTED_ARGS 3
#define ITERATIONS 1000

/*
 * Using PAPI library to count comparisons during execution.
 * Sadly it seems that PAPI is rather Leaky wrt to memory, so be careful when running with scripts 
 * Fheap operations are all leak free. 
 *
 */

void papi_insert_heap(Vertex *A, int n, int *events, int eventsize, long long *values)
{
  Heap *H = MakeHeap(A,n,n+ITERATIONS);

  

  int index = 0;
  long long comparisons = 0;
  for(index = 0; index < ITERATIONS; index++)
    {
      /* begin counting */
      init_PAPI(events, eventsize);
      /* do work */
      Insert(H,A[index].v,A[index].k);
      /* stop counting */
      stop_PAPI(values,eventsize);
      DeleteMin(H);
      comparisons += values[0];
    }
  values[0] = comparisons;
  

  DestroyHeap(H);
}

void papi_findmin_heap(Vertex *A, int n, int *events, int eventsize, long long *values)
{
  Heap *H = MakeHeap(A,n,n+ITERATIONS);
  Vertex temp;
  int index = 0;

  init_PAPI(events, eventsize);

  for(index = 0; index < ITERATIONS; index++)
    {
      temp = FindMin(H);
    }

  stop_PAPI(values, eventsize);

  temp.k++;

  DestroyHeap(H);
}

void papi_decrease_key_heap(Vertex *A, int n, int *events, int eventsize, long long *values)
{
  Heap *H = MakeHeap(A,n,n+ITERATIONS);

  int index = 0;

  init_PAPI(events, eventsize);

  for(index = 0; index < ITERATIONS; index++)
    {
	DecreaseKey(H,index % n,H->A[H->Dict[index % n]].k/2);
    }

  stop_PAPI(values, eventsize);

  DestroyHeap(H);
}

void papi_extract_min_heap(Vertex *A, int n, int *events, int eventsize, long long *values)
{
  Heap *H = MakeHeap(A,n,n+ITERATIONS);
  Vertex temp;
  int index = 0;
  long long comparisons = 0;
  for(index = 0; index < ITERATIONS; index++)
    {
      /* begin counting */
      init_PAPI(events, eventsize);
      /* do work */
      temp = DeleteMin(H);
      /* stop counting */
      stop_PAPI(values,eventsize);

      Insert(H,A[index].v,A[index].k);
      comparisons += values[0];
    }
  values[0] = comparisons;

  temp.k++;
  DestroyHeap(H);
}

void papi_create_heap(Vertex *A, int n, int *events, int eventsize, long long *values)
{
  init_PAPI(events, eventsize);

  Heap *H = MakeHeap(A,n,n+ITERATIONS);

  stop_PAPI(values,eventsize);

  DestroyHeap(H);
}

long long average(long long *A, long size)
{
  int index = 0;
  long long sum = 0;
  for(index = 0; index < size; index++)
    {
      sum += A[index];
    }
  return sum/size;
}

void setup_vertex_array(Vertex *A, int n)
{
  int index = 0;
  for(index = 0; index < n; index++)
    {
      A[index] = MakeVertex(index, (float) rand());
    }

}


int main(int argc, char **argv)
{
  if(argc < EXPECTED_ARGS){
    printf("Usage: %s n (elements) k (iterations)\n",argv[0]);
    exit(1);
  }


  /* setup */
  srand(time(NULL));

  int n = atoi(argv[1]);
  int k = atoi(argv[2]);

  printf("#----Papi counting L2 cache misses, results for heap at n = %d, k = %d, iter = %d----\n",n,k,ITERATIONS);
  printf("#n \tmakeheap \tinsert \t\tdeleteMin \tdecreaseKey \tfindMin\n");

  /* papi stuff */
  const int eventsize = 1;
  long long values[2]; 
  int *events = malloc(sizeof(int));
  if(!events) exit(1);
  //events[0] = PAPI_BR_CN;
  //events[0] = PAPI_L1_DCM;
  events[0] = PAPI_L2_DCM;
  long long create_cond_count[k];
  long long insert_cond_count[k];
  long long find_min_cond_count[k];
  long long decrease_key_cond_count[k];
  long long extract_min_cond_count[k];


  int index = 0;
  for(index = 0; index < k; index++)
    {
      int array_size = n < ITERATIONS ? ITERATIONS : n;
      Vertex *test_array = calloc(sizeof(Vertex),array_size+ITERATIONS);
      setup_vertex_array(test_array,array_size+ITERATIONS);

      papi_create_heap(test_array,n,events,eventsize,values);
      create_cond_count[index] = values[0];

      free(test_array);
      test_array = calloc(sizeof(Vertex),array_size+ITERATIONS);
      setup_vertex_array(test_array,array_size+ITERATIONS);

      papi_insert_heap(test_array, n, events, eventsize, values);
      insert_cond_count[index] = values[0];

      free(test_array);
      test_array = calloc(sizeof(Vertex),array_size+ITERATIONS);
      setup_vertex_array(test_array,array_size+ITERATIONS);

      papi_findmin_heap(test_array, n, events, eventsize, values);
      find_min_cond_count[index] = values[0];

      free(test_array);
      test_array = calloc(sizeof(Vertex),array_size+ITERATIONS);
      setup_vertex_array(test_array,array_size+ITERATIONS);

      papi_decrease_key_heap(test_array, n, events, eventsize, values);
      decrease_key_cond_count[index] = values[0];

      free(test_array);
      test_array = calloc(sizeof(Vertex),array_size+ITERATIONS);
      setup_vertex_array(test_array,array_size+ITERATIONS);

      papi_extract_min_heap(test_array, n, events, eventsize, values);
      extract_min_cond_count[index] = values[0];
      
      free(test_array);
    }

  long long avrg_create = average(create_cond_count,k); 
  long long avrg_insert = average(insert_cond_count,k);
  long long avrg_find_min = average(find_min_cond_count,k);
  long long avrg_decrease_key = average(decrease_key_cond_count,k);
  long long avrg_extract_min = average(extract_min_cond_count,k);

  printf("%d \t%llu \t%llu \t%llu \t%llu \t%llu\n",n,avrg_create,avrg_insert,avrg_extract_min,avrg_decrease_key,avrg_find_min);

  free(events);

  return 0;
}
