#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <papi.h>

#include <AdvAlgDat/fheap.h>
#include <AdvAlgDat/papi_wrap.h>

#define DEF_V 0
#define EXPECTED_ARGS 3

/*
 * Using PAPI library to count comparisons during execution.
 * Sadly it seems that PAPI is rather Leaky wrt to memory, so be careful when running with scripts 
 * Fheap operations are all leak free. 
 *
 */



void papi_insert_fheap(Vertex *A, int n, int *events, int eventsize, long long *values, int iterations)
{
	fheap *fh = fheap_create(n+iterations);

	node *temp = NULL;

	long long comparisons = 0;

	int index = 0;
	for(index = 0; index < n; index++)
	{
		temp = fheap_insert(fh,A[index]);
	}

	for(index = 0; index < iterations; index++)
	{
		init_PAPI(events, eventsize);

		temp = fheap_insert(fh,A[index]);		

		stop_PAPI(values,eventsize);

		comparisons += values[0];

		fheap_extract_min(fh);
	}
	
	values[0] = comparisons;
	float rc = GET_KEY(temp);
	rc++;

	fheap_destroy(fh);
}

void papi_findmin_fheap(Vertex *A, int n, int *events, int eventsize, long long *values, int iterations)
{
	fheap *fh = fheap_create(n);

	node *temp = NULL;

	int index = 0;
	for(index = 0; index < n; index++)
	{
		temp = fheap_insert(fh, A[index]);
	}

	init_PAPI(events, eventsize);

	for(index = 0; index < iterations; index++)
	{
		temp = fheap_find_min(fh);
	}

	stop_PAPI(values, eventsize);

	float rc = GET_KEY(temp);
	rc++;
	fheap_destroy(fh);
}

void papi_decrease_key_fheap(Vertex *A, int n, int *events, int eventsize, long long *values, int iterations)
{
	fheap *fh = fheap_create(n);

	node **node_array = malloc(sizeof(*node_array) * (n));
	
	int index = 0;
	for(index = 0; index < n; index++)
	{
		node_array[index] = fheap_insert(fh,A[index]);
	}

	int rc = 0;

	init_PAPI(events, eventsize);
	for(index = 0; index < iterations; index++)
	{
		rc = fheap_decrease_key(fh,node_array[index],GET_KEY(node_array[index])/2);
	}
	stop_PAPI(values, eventsize);

	rc++;

	if(node_array) free(node_array);
	fheap_destroy(fh);
}

void papi_extract_min_fheap(Vertex *A, int n, int *events, int eventsize, long long *values, int iterations)
{
	fheap *fh = fheap_create(n+iterations);
	node *temp = NULL;

	long long comparisons = 0;

	int index = 0;
	for(index = 0; index < n; index++)
	{
		temp = fheap_insert(fh, A[index]);
	}

	int array_index = index;

	for(index = 0; index < iterations; index++)
	{

		init_PAPI(events, eventsize);

		temp = fheap_extract_min(fh);
	
		stop_PAPI(values, eventsize);
		comparisons += values[0];
		fheap_insert(fh,A[array_index+index]);
	}

	values[0] = comparisons;
	float rc = GET_KEY(temp);
	rc++;

	fheap_destroy(fh);
}

void papi_create_fheap(Vertex *A, int n, int *events, int eventsize, long long *values)
{
	int index = 0;
	init_PAPI(events, eventsize);

	fheap *fh = fheap_create(n);

	for(index = 0; index < n; index++)
	{
		fheap_insert(fh,A[index]);
	}

	stop_PAPI(values,eventsize);

	fheap_destroy(fh);
}

long long average(long long *A, long size)
{
	int index = 0;
	long long sum = 0;
	for(index = 0; index < size; index++)
	{
		sum += A[index];
	}
	return sum/size;
}

void setup_vertex_array(Vertex *A, int n)
{
	int index = 0;
	for(index = 0; index < n; index++)
	{
		A[index] = MakeVertex(index, (float) rand());
	}

}


int main(int argc, char **argv)
{
	if(argc < EXPECTED_ARGS){
		printf("Usage: %s n (elements) k (iterations)\n",argv[0]);
		exit(1);
	}


	/* setup */
	srand(time(NULL));

	int n = atoi(argv[1]);
	int k = atoi(argv[2]);
	int single_test_iterations = 1000;
	
	printf("#----Papi counting L2 cache misses, results for fheap at n = %d, k = %d, iter = %d----\n",n,k,single_test_iterations);
	printf("#n \tmakeHeap \tinsert \t\tdeleteMin \tdecreaseKey \tfindMin\n");

	/* papi stuff */
	const int eventsize = 1;
	long long values[2]; 
	int *events = malloc(sizeof(int));
	if(!events) exit(1);
	//events[0] = PAPI_BR_CN;
	//events[0] = PAPI_L1_DCM;
	events[0] = PAPI_L2_DCM;


	long long create_cond_count[k];
	long long insert_cond_count[k];
	long long find_min_cond_count[k];
	long long decrease_key_cond_count[k];
	long long extract_min_cond_count[k];


	int index = 0;
	for(index = 0; index < k; index++)
	{
		Vertex *test_array = calloc(sizeof(Vertex),n+single_test_iterations);
		setup_vertex_array(test_array,n+single_test_iterations);

		papi_create_fheap(test_array,n,events,eventsize,values);
		create_cond_count[index] = values[0];

		papi_insert_fheap(test_array, n, events, eventsize, values, single_test_iterations);
		insert_cond_count[index] = values[0];

		papi_findmin_fheap(test_array, n, events, eventsize, values,single_test_iterations);
		find_min_cond_count[index] = values[0];

		papi_decrease_key_fheap(test_array, n, events, eventsize, values,single_test_iterations);
		decrease_key_cond_count[index] = values[0];

		papi_extract_min_fheap(test_array, n, events, eventsize, values,single_test_iterations);
		extract_min_cond_count[index] = values[0];
		free(test_array);
	}

	long long avrg_create = average(create_cond_count,k); 
	long long avrg_insert = average(insert_cond_count,k);
	long long avrg_find_min = average(find_min_cond_count,k);
	long long avrg_decrease_key = average(decrease_key_cond_count,k);
	long long avrg_extract_min = average(extract_min_cond_count,k);

	printf("%d \t%llu \t%llu \t%llu \t%llu \t%llu\n",n,avrg_create,avrg_insert,avrg_extract_min,avrg_decrease_key,avrg_find_min);

	free(events);

	return 0;
}
