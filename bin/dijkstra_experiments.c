#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <AdvAlgDat/Dijkstra.h>
#include <AdvAlgDat/graph_util.h>

#define EXPECTED_ARGS 3

void print_path(Path *p)
{
  int i;
  printf("\nDistance array:\n");
  for (i = 0; i < p->n; i++)
    {
      printf("%f, ", p->dist[i]);
    }
  printf("\nPrev array:\n");
  for (i = 0; i < p->n; i++)
    {
      printf("%d, ", p->prev[i]);
    }
}

double time_dijkstra_sparse_fheap(Graph *G)
{	
  clock_t begin, end;
  double time_spent;

  /* start counting */
  begin = clock();

  /* do work */
  Path *p = DijkstraFibHeap(G, 0);
		  
  /* stop counting */
  end = clock();
  //printf("\n\n PATH FOR SPARSE FHEAP");
  //print_path(p);

  /* use p so O2 doesnt remove call due to unused */
  float key = p->dist[0];
  key++;
  /* cleanup */
  DestroyPath(p);

  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

  return time_spent;
}

double time_dijkstra_dense_fheap(Graph *G)
{	
  clock_t begin, end;
  double time_spent;

  //Graph *G = MakeRandomGraph(size,0.9);
  /* start counting */
  begin = clock();

  /* do work */
  Path *p = DijkstraFibHeap(G, 0);
		  
  /* stop counting */
  end = clock();
  //printf("\n\n PATH FOR DENSE FHEAP");
  //print_path(p);

  /* use p so O2 doesnt remove call due to unused */
  float key = p->dist[0];
  key++;
  /* cleanup */
  DestroyPath(p);

  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

  return time_spent;
}


double time_dijkstra_sparse_binheap(Graph *G)
{	
  clock_t begin, end;
  double time_spent;

  //Graph *G = MakeRandomGraph(size,0.1);
  /* start counting */
  begin = clock();

  /* do work */
  Path *p = DijkstraBinHeap(G, 0);
  
  
  /* stop counting */
  end = clock();
  //printf("\n\n PATH FOR SPARSE BINHEAP");
  //print_path(p);

  /* use p so O2 doesnt remove call due to unused */
  float key = p->dist[0];
  key++;
  /* cleanup */
  DestroyPath(p);

  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

  return time_spent;
}

double time_dijkstra_dense_binheap(Graph *G)
{	
  clock_t begin, end;
  double time_spent;

  //Graph *G = MakeRandomGraph(size,0.9);
  /* start counting */
  begin = clock();

  /* do work */
  Path *p = DijkstraBinHeap(G, 0);
		  
  /* stop counting */
  end = clock();
  //printf("\n\n PATH FOR DENSE BINHEAP");
  //print_path(p);

  /* use p so O2 doesnt remove call due to unused */
  float key = p->dist[0];
  key++;
  /* cleanup */
  DestroyPath(p);

  time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

  return time_spent;
}

float average(float *A, int size)
{
	int index = 0;
	float sum = 0;
	for(index = 0; index < size; index++)
	{
		sum += A[index];
	}

	return sum / size;
}

long long average_long(long long *A, int size)
{
	int index = 0;
	long long sum = 0;
	for(index = 0; index < size; index++)
	{
		sum += A[index];
	}
	return sum / (long long) size;
}

int main(int argc, char **argv)
{

  if(argc < EXPECTED_ARGS){
    printf("Usage: %s n (elements) k (iterations)\n",argv[0]);
    exit(1);
  }

  srand(time(NULL));

  int n = atoi(argv[1]);
  int k = atoi(argv[2]);

  float time_sparse_fib[k],time_sparse_bin[k],time_dense_fib[k],time_dense_bin[k];
  long long sparse_edges[k],dense_edges[k];
  printf("#----Starting experiments for n = %d, k = %d----\n",n,k);

  int iterations = 0;
  for(iterations = 0; iterations < k; iterations++)
    {
      Graph *G_sparse = MakeRandomGraph(n,2.0/(float)n);
      sparse_edges[iterations] = G_sparse->edges;
      time_sparse_fib[iterations] = time_dijkstra_sparse_fheap(G_sparse);
      time_sparse_bin[iterations] = time_dijkstra_sparse_binheap(G_sparse);
      DestroyGraph(G_sparse);

      Graph *G_dense = MakeRandomGraph(n,0.9);
      dense_edges[iterations] = G_dense->edges;
      time_dense_fib[iterations] = time_dijkstra_dense_fheap(G_dense);
      time_dense_bin[iterations] = time_dijkstra_dense_binheap(G_dense);
      DestroyGraph(G_dense);
    }

  float avrg_time_sparse_fib = average(time_sparse_fib,k);
  float avrg_time_sparse_bin = average(time_sparse_bin,k);
  float avrg_time_dense_fib = average(time_dense_fib,k);
  float avrg_time_dense_bin = average(time_dense_bin,k);
  long long avrg_sparse_edges = average_long(sparse_edges,k);
  long long avrg_dense_edges = average_long(dense_edges,k);
  printf("#n \t\tSparseFibHeap \t\tSparseBinHeap \t\tedgesSparse \t\tDenseFibHeap \t\tDenseBinHeap \t\tedgesDense\n");
  printf("%d \t\t%f \t\t%f \t\t%lld \t\t%f \t\t%f \t\t%lld\n",n,avrg_time_sparse_fib,avrg_time_sparse_bin,avrg_sparse_edges,avrg_time_dense_fib,avrg_time_dense_bin,avrg_dense_edges);

  return 0;  
}
