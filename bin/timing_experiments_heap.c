#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <AdvAlgDat/Heap.h>
#include <AdvAlgDat/Vertex.h>

#define DEF_V 0
#define EXPECTED_ARGS 3
#define ITERATIONS 1000

double time_make_heap(Vertex *A, int n)
{
	clock_t begin,end;
	double time_spent;

	/* begin counting */
	begin = clock();

	/* do work */
	Heap *H = MakeHeap(A,n,n);

	/* stop counting */
	end = clock();

	/* cleanup */
	DestroyHeap(H);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

double time_insert_heap(Vertex *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	Heap *H = MakeHeap(A,n,n+1);

	int index = 0;

	

	
	for(index = 0; index < ITERATIONS; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  Insert(H,A[index].v,A[index].k);
	  /* stop counting */
	  end = clock();
	  DeleteMin(H);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}

	

	/* cleanup */
	DestroyHeap(H);

	return time_spent;
}

double time_find_min_heap(Vertex *A, int n)
{
	clock_t begin,end;
	double time_spent;

	Heap *H = MakeHeap(A,n,n);

	int index = 0;
	Vertex temp;	

	/* begin counting */
	begin = clock();

	/* do work */
	for(index = 0; index < ITERATIONS; index++)
	{
		temp = FindMin(H);
	}

	/* stop counting */
	end = clock();
	temp.k++;

	/* cleanup */
	DestroyHeap(H);

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

double time_delete_min_heap(Vertex *A, int n)
{
	clock_t begin,end;
	double time_spent = 0.0;

	Heap *H = MakeHeap(A,n,n);

	int index = 0;
	Vertex temp;	

        for(index = 0; index < ITERATIONS; index++)
	{
	  /* begin counting */
	  begin = clock();
	  /* do work */
	  temp = DeleteMin(H);
	  /* stop counting */
	  end = clock();
	  Insert(H,A[index].v,A[index].k);
	  time_spent += (double)(end - begin)/CLOCKS_PER_SEC;
	}
	temp.k++;

	/* cleanup */
	DestroyHeap(H);

	return time_spent;
}

double time_decrease_key_heap(Vertex *A, int n)
{
	clock_t begin,end;
	double time_spent;

	Heap *H = MakeHeap(A,n,n);

	int index = 0;
	/* begin counting */
	begin = clock();

	/* do work */
	for(index = 0; index < ITERATIONS; index++)
	{
		DecreaseKey(H,index,H->A[H->Dict[index]].k/2);
	}

	/* stop counting */
	end = clock();

	/* cleanup */
	DestroyHeap(H);
	
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	return time_spent;
}

void setup_vertex_array(Vertex *V, int n)
{
	int index = 0;
	for(index = 0; index < n; index++)
	{
		V[index] = MakeVertex(index, (float) rand());
	}
}

float average(float *A, int size)
{
	int index = 0;
	float sum = 0;
	for(index = 0; index < size; index++)
	{
		sum += A[index];
	}

	return sum / size;
}

int main(int argc, char **argv)
{
	if(argc < EXPECTED_ARGS){
		printf("Usage: %s n (elements) k (iterations)\n",argv[0]);
		exit(1);
	}

	/* setup stuff */
	srand(time(NULL));

	int n = atoi(argv[1]);
	int k = atoi(argv[2]);

	float time_insert[k];
	float time_deletemin[k];
	float time_decreasekey[k];
	float time_heap_create[k];
	float time_heap_find_min[k];

	printf("#----Timing results for heap at n = %d, k = %d----\n",n,k);
	printf("#n \tmakeheap \tinsert \tdeleteMin \tdecreaseKey \tFindMin\n");

	int iterations = 0;
	for(iterations = 0; iterations < k; iterations++)
	  {
	    int array_size = n < ITERATIONS ? ITERATIONS : n;			
	    Vertex *test_array = calloc(sizeof(Vertex),array_size);
	    setup_vertex_array(test_array,array_size);
	    time_heap_create[iterations] = time_make_heap(test_array, n);
	    free(test_array);

	    test_array = calloc(sizeof(Vertex),array_size);
	    setup_vertex_array(test_array,array_size);
	    time_insert[iterations] = time_insert_heap(test_array, n);
	    free(test_array);

	    test_array = calloc(sizeof(Vertex),array_size);
	    setup_vertex_array(test_array,array_size);
	    time_heap_find_min[iterations] = time_find_min_heap(test_array, n);
	    free(test_array);

	    test_array = calloc(sizeof(Vertex),array_size);
	    setup_vertex_array(test_array,array_size);
	    time_decreasekey[iterations] = time_decrease_key_heap(test_array, n);
	    free(test_array);

	    test_array = calloc(sizeof(Vertex),array_size);
	    setup_vertex_array(test_array,array_size);
	    time_deletemin[iterations] = time_delete_min_heap(test_array,n);
	    free(test_array);
	  }

	float avrg_insert_time = average(time_insert,k);
	float avrg_delete_min = average(time_deletemin,k);
	float avrg_decrease_key = average(time_decreasekey,k);
	float avrg_heap_create = average(time_heap_create,k);
	float avrg_heap_find_min = average(time_heap_find_min,k);

	printf("%d \t%f \t%f \t%f \t%f \t%f\n",n,avrg_heap_create,avrg_insert_time,avrg_delete_min,avrg_decrease_key,avrg_heap_find_min);

	return 0;
}
