
#include <AdvAlgDat/graph_util.h>
#include <time.h>
#include <stdlib.h>

float RandFloat()
{
  return (float)rand()/(float)RAND_MAX;
}

Graph *MakeRandomGraph(int size, float density)
{
  Graph *G = malloc(sizeof(*G));
  G->n = size;
  G->edges = 0;
  G->weights = malloc(G->n * sizeof(float*));
  int i,j;
  for (i = 0; i < G->n; i++)
    {
      G->weights[i] = malloc(G->n * sizeof(float));
      for (j = 0; j < G->n; j++)
	{
	  if (j != i) G->weights[i][j] = NO_EDGE;
	  else G->weights[i][j] = SAME_NODE;
	}
    }

  for (i = 1; i < G->n; i++)
    {
      int edge = rand()%i;
      // Make an undirected edge to a node in the graph.
      G->weights[i][edge] = RandFloat();
      G->weights[edge][i] = G->weights[i][edge];
      G->edges++;
      for (j = 0; j < i; j++)
	{
	  if (j != edge)
	    {
	      if (RandFloat() <= density)
		{
		  G->weights[i][j] = RandFloat();
		  G->weights[j][i] = G->weights[i][j];
		  G->edges++;
		}
	    }
	}
    }

  return G;
  
}
