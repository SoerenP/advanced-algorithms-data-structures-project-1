#ifndef _vertex_h_
#define _vertex_h_


typedef struct Vertex
{
  int v;
  float k;
} Vertex;

Vertex MakeVertex(int v, float k);

#endif
