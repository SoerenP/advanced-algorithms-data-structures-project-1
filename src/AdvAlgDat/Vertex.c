#include <AdvAlgDat/Vertex.h>

Vertex MakeVertex(int v, float k)
{
  Vertex res;
  res.v = v;
  res.k = k;
  return res;
}
