#ifndef _dijkstra_h
#define _dijkstra_h
#include <AdvAlgDat/Vertex.h>

#define SAME_NODE (0.0)
#define NO_EDGE (-1.0)

typedef struct graph {
  int n;
  float **weights;
  long long edges;
} Graph;

typedef struct path {
  int n;
  float *dist;
  int *prev;
} Path;

Path *DijkstraBinHeap(Graph *G, int source);

Path *DijkstraFibHeap(Graph *G, int source);

void DestroyGraph(Graph *G);

void DestroyPath(Path *P);
  

#endif
