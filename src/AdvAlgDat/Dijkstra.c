#include <AdvAlgDat/Dijkstra.h>
#include <AdvAlgDat/Heap.h>
#include <AdvAlgDat/fheap.h>
#include <AdvAlgDat/Vertex.h>
#include <AdvAlgDat/dbg.h>
#include <AdvAlgDat/bool.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

Path *DijkstraBinHeap(Graph *G, int source)
{
  Path *res = malloc(sizeof(*res));
  res->n = G->n;
  res->dist = malloc(G->n * sizeof(float));
  res->prev = malloc(G->n * sizeof(Vertex));

  Vertex *A = malloc(G->n * sizeof(*A));
  
  Heap *Q = MakeHeap(A,0,G->n);

  res->dist[source] = 0;
  
  int v;
  //int inserts = 0, deleteMins = 0, decreaseKeys = 0;
  for (v = 0; v < G->n; v++)
    {
      if (v != source)
	{
	  res->dist[v] = INFINITY;
	}
      Insert(Q, v, res->dist[v]);

      //inserts++;
    }

  while (Q->n > 0)
    {
      Vertex u = DeleteMin(Q);
      //deleteMins++;
      Q->Dict[u.v] = -1;
      for (v = 0; v < G->n; v++)
	{
	  if (Q->Dict[v] > -1 && G->weights[u.v][v] > NO_EDGE)
	    {
	      float alt = res->dist[u.v] + G->weights[u.v][v];
	      if (alt < res->dist[v])
		{
		  res->dist[v] = alt;
		  res->prev[v] = u.v;
		  DecreaseKey(Q,v,alt);
		  //decreaseKeys++;
		}
	    }
	}
    }

 // printf("#Dijkstra for BinHeap finished with %d inserts, %d deleteMins and %d decreaseKeys, %d edges in graph\n", inserts, deleteMins, decreaseKeys, G->edges);

  /* cleanup */
  if(A) free(A);
  if(Q) DestroyHeap(Q);
  return res;
}

Path *DijkstraFibHeap(Graph *G, int source)
{
  Path *res = malloc(sizeof(*res));
  res->n = G->n;
  res->dist = malloc(G->n * sizeof(float));
  res->prev = malloc(G->n * sizeof(Vertex));

  Vertex *A = malloc(G->n * sizeof(*A));

  fheap *Q = fheap_create(G->n); //remove this hardcoding and take as input

  res->dist[source] = 0;
  
  node **Dict = malloc((sizeof(*Dict))*G->n);
   
  int v;
  //int inserts = 0, deleteMins = 0, decreaseKeys = 0;
  for (v = 0; v < G->n; v++)
    {
      if (v != source)
	{
	  res->dist[v] = INFINITY;
	}
      Vertex vert;
      vert.v = v;
      vert.k = res->dist[v];
      Dict[v] = fheap_insert(Q, vert);
      //inserts++;
    }

  
  while (Q->node_count > 0)
    {
      node *u = fheap_extract_min(Q);
      //deleteMins++;
      Dict[u->v.v] = NULL;
      for (v = 0; v < G->n; v++)
	{
	  if (Dict[v] != NULL && G->weights[u->v.v][v] > NO_EDGE)
	    {
	      float alt = res->dist[u->v.v] + G->weights[u->v.v][v];
	      
	      if (alt < res->dist[v])
		{
		  res->dist[v] = alt;

		  res->prev[v] = u->v.v;
		  
		  
		  fheap_decrease_key(Q,Dict[v],alt);
		  //decreaseKeys++;
		}
	    }
	} 
    }

  //printf("#Dijkstra for FibHeap finished with %d inserts, %d deleteMins and %d decreaseKeys\n", inserts, deleteMins, decreaseKeys);
	
  if(A) free(A);
  if(Dict) free(Dict);
  if(Q) fheap_destroy(Q);
  return res;
}

void DestroyGraph(Graph *G)
{
  if (G)
    {
      if (G->weights)
	{
	  int i;
	  for (i = 0; i < G->n; i++)
	    {
	      if (G->weights[i]) free(G->weights[i]);
	    }
	  free(G->weights);
	}
      free(G);
    }
}

void DestroyPath(Path *P)
{
  if (P)
    {
      if (P->dist) free(P->dist);
      if (P->prev) free(P->prev);
      free(P);
    }
}
  

