/*
 * Fibonacci heap implementation inspired by 
 * "Fibonacci heaps and improved network optimization algorithms"
 * The implementation is static, in that you create an fheap 
 * with an upper amount of nodes n. If you try to insert more
 * an error is thrown. Can easily be extended to the dynamic case
 * in the future, but was not neccesarry for our purpose (dijkstra's
 * shortes path. 
 * All nodes are deleted when the fheap is destroyed, also dangling
 * pointers from extract_min (CAREFUL ABOUT THIS!)
 *
 * Node also that Nodes (hue hue hue ) holds Vertices with a key and
 * vertix indiacator, as such this implementation is not "all-purpose"
 * unless you are willin to waste a int field for every key you save
 * in the general case. 
 *
*/

#include <stdlib.h>
#include <math.h>

#include <AdvAlgDat/dbg.h>
#include <AdvAlgDat/bool.h>
#include <AdvAlgDat/fheap.h>
#include <AdvAlgDat/Vertex.h>

static node *node_create(fheap *fh, Vertex v)
{
	node *n = fh->node_pool[fh->node_index];
	check_mem(n);

	//n->key = key;
	n->v = v;
	n->rank = 0;
	n->parent = NULL;
	n->child = NULL;
	n->left = n; //This is needed for traversing DLL properly
	n->right = n; //This is needed for traversing DLL properly
	n->mark = FALSE; 

	return n;
error:
	return NULL;
}

fheap *fheap_create(int n)
{
	fheap *fh = malloc(sizeof(*fh));
	check_mem(fh);

	fh->node_pool = malloc(sizeof(node *) * n);
	int index = 0;
	for(index = 0; index < n; index++)
	{
		fh->node_pool[index] = malloc(sizeof(*fh->node_pool[index]));
	}

	check_mem(fh->node_pool);

	fh->root = NULL;
	fh->melding_list = NULL;
	fh->node_count = 0;
	fh->node_index = 0;
	fh->node_pool_size = n;

	return fh;
error:
	return NULL;
}

void fheap_destroy(fheap *fh)
{
	check(fh != NULL, "Recieved NULL pointer.");
	
	if(fh->node_pool){
		int index = 0;
		for(index = 0; index < fh->node_pool_size; index++)
		{
			free(fh->node_pool[index]);
		}
		free(fh->node_pool);
	}

	free(fh);
	fh = NULL;
error:
	return;
}

/* Insert single node into DLL root list */
static int fheap_rootlist_insert(fheap *fh, node *n)
{
	check(fh != NULL && n != NULL, "Recieved NULL pointers.");

	/* not the new root */
	if(GET_KEY(n) > GET_KEY(fh->root)){
		n->right = fh->root->right;
		n->right->left = n;
		fh->root->right = n;
		n->left = fh->root;
	} else {
	/* the node is the new root of FHeap */
		n->right = fh->root;
		n->left = fh->root->left;
		fh->root->left->right = n;
		fh->root->left = n;
		fh->root = n;
	}
	n->parent = NULL; //All roots have no parents 

	return FHEAP_OK;
error:
	return FHEAP_ERR;
}

static int node_childlist_insert(node *child, node *parent)
{
	check(child != NULL && parent != NULL, "Recieved NULL pointers.");

	//insert child to the right of one of parent's children. 
	node *temp = parent->child; //get some child of parent.
	
	if(temp != NULL){
		child->right = temp->right;
		temp->right->left = child;
		child->left = temp;
		temp->right = child;
	} else {
		parent->child = child;
		child->left = child;
		child->right = child;
	}
	child->parent = parent;
	parent->rank++;

	return FHEAP_OK;
error:
	return FHEAP_ERR;
}

/* Remove from DLL list */
/* leave n's pointers untouched */
static inline void double_linked_list_delete(node *n)
{
	n->left->right = n->right;
	n->right->left = n->left;
}

node *fheap_insert(fheap *fh, Vertex v)
{
	check(fh != NULL, "Recieved NULL pointer.");

	check(fh->node_index < fh->node_pool_size, "Out of nodes for fheap.");

	node *new = node_create(fh,v);
	fh->node_index++;
	check(new != NULL, "Failed to create Node.");

	if(fh->root != NULL)
	{
		int rc = fheap_rootlist_insert(fh,new);
		check(rc != FHEAP_ERR, "Failed to insert into rootlist.");
	} 
	else /* no root yet */
	{
		fh->root = new;
	}
	
	fh->node_count++;

	return new;
error:
	return NULL; 
}

static int fheap_link(node *child, node *min)
{
	check(child != NULL && min != NULL, "Recieved NULL pointers.");

	//remove child from rootlist of fh. 
	double_linked_list_delete(child);
	
	//Make child a child of min
	node_childlist_insert(child,min);

	child->mark = FALSE;

	return FHEAP_OK;
error:
	return FHEAP_ERR;
}

/* Creates array and puts heaps of rank into array[rank] */
/* when there are collisions in said array, we link heaps */
/* keep going until no more collisions 			*/
/* When done, find the new minimum (root) of the FHeap */
static int fheap_consolidate(fheap *fh)
{
	check(fh != NULL, "Recieved NULL pointer.");

	//Need array, of size D(n) <= floor( log_goldenratio n)
	//The maximum degree is  bounded s.t we know the size of the array (the max possible degree after consolidation)
	//thus needs to be at most this big (instead of just taking node->count to be sure)
	int size = (int)(floor((log((double)fh->node_count)/log((double)2))))+2;

	//Init to null, use calloc!
	fh->melding_list = calloc(size,sizeof(node *));
	check_mem(fh->melding_list);

	//Setup variables
	node *iterator = fh->root;
	node *next = iterator;
	node *x = NULL;
	do
	{
		x = next;
		int rank = x->rank;
		//If the rank is occupied, but by ourselves, we are done for this rank.
		if(fh->melding_list[rank] != x)
		{
			//If index already occupied by another heap, we need to Link the heaps of same rank
			while(fh->melding_list[rank] != NULL)
			{
				//Grab the heap of same degree
				node *y = fh->melding_list[rank];

				//Make sure "x" is the root for easier code later 
				if(GET_KEY(x) > GET_KEY(y))
				{
					node *temp = next;
					x = y;
					y = temp;
				}

				//Make y a child of x (x should always be root)
				int rc = fheap_link(y,x);
				check(rc != FHEAP_ERR, "Failed to link.");
				iterator = x;
				next = x;

				//After linking, we know that the resulting root has rank++
				fh->melding_list[rank] = NULL; 
				rank++;
			}
			//Since we always make x the root in the new heap, we can always set it into the array
			fh->melding_list[rank] = x;
		}
		//next node
		next = next->right; 
	
	//Until we have gone through entire rootlist.
	} while(next != iterator && next != NULL);
	fh->root = NULL;

	/* Find the new minimum of the entire heap. Scan through it */
	fh->root = iterator;
	next = iterator;
	do
	{
		if(GET_KEY(next) < GET_KEY(fh->root))
		{
			fh->root = next;
		}
		next = next->right;
	} while(next != iterator);

	//Cleanup the array of the ranks
	if(fh->melding_list) free(fh->melding_list);

	return FHEAP_OK;
error:
	return FHEAP_ERR;
}

/* Finds the min element (easy) and consolidates if neccesary (hard) */
/* The returned node will be garbage collected by the heap_destroy */
/* So no need to free it when you are done with it! */
node *fheap_extract_min(fheap *fh)
{
	check(fh != NULL, "Recieved NULL pointer.");

	node *min = fh->root;

	if(min != NULL) {
		node *min_child = min->child;
		if(min_child != NULL){

			//Remove all children of the root from roots child list, and add to root list of heap.
			node *next = min_child;
			do
			{
				next->parent = NULL;
				next = next->right;
			} while(next != min_child);	

			//Add children to the root list (double linked yay!)
			min->left->right = min_child->right;
			min_child->right->left = min->left;
			min->left = min_child;
			min_child->right = min;
		}

		//Remove min fom rootlist of heap, without changing min's pointers.

		double_linked_list_delete(min);


		//only element? Nothing left afterwards then
		if(min->right == min)
		{
			fh->root = NULL;
		}
		else
		{
			//Not neccesarely correct root, but consolidate finds the correct one, but some root must be set
			fh->root = min->right;
			int rc = fheap_consolidate(fh);
			check(rc != FHEAP_ERR, "Consolidate failed");
		} 

		//After extraction, one less element.
		fh->node_count--;
	}

	return min;
error:
	return NULL;
}

static int cut(fheap *fh, node *child, node *parent)
{
	/* Remove child from child list of parent */
	check(fh != NULL && child != NULL && parent != NULL, "Recieved NULL pointers.");	

	if(child->right == child) //only child?
	{
		parent->child = NULL; //no children afterwards
	} else {
		parent->child = child->right; //Make sure that child is set to something
	}

	double_linked_list_delete(child);
	parent->rank--;

	/* add child to rootlist of fh */
	fh->root->right->left = child;
	child->right = fh->root->right;
	fh->root->right = child;
	child->left = fh->root;
	child->parent = NULL;

	/* unmark child */
	child->mark = FALSE;

	return FHEAP_OK;
error:
	return FHEAP_ERR;
}

static int cascading_cut(fheap *fh, node *n)
{
	check(fh != NULL && n != NULL, "Recived NULL pointers.");

	node *z = n->parent;
	/* only cascading cut if n is not a root */
	if(z != NULL)
	{
		if(n->mark == FALSE){
			n->mark = TRUE;
		} else {
			cut(fh,n,z);
			int rc = cascading_cut(fh,z);
			check(rc != FHEAP_ERR, "Failed to cascade.");
		}
	}

	return FHEAP_OK;
error:
	return FHEAP_ERR;
}

/* assumes a pointer to the target node for decreasing. sets key to new_key */
int fheap_decrease_key(fheap *fh, node *target, float new_key)
{
	check(new_key <= GET_KEY(target), "New key is greater than current key.");

	check(fh != NULL && target != NULL, "Recieved NULL pointers.");
	check(fh->root != NULL, "NULL root.");

	SET_KEY(target) = new_key;
	node *y = target->parent;

	if(y != NULL && GET_KEY(target) < GET_KEY(y))
	{
		/* cut target from childlist of y, and add to rootlist of fh */
		int rc = cut(fh,target,y);
		check(rc != FHEAP_ERR, "Failed to cut.");

		/* we might need to cleanup based on the marks */
		rc = cascading_cut(fh,y);
		check(rc != FHEAP_ERR, "Failed to cascading cut.");
	}
        //printf("Target key is %f\n", GET_KEY(target));
	//printf("Root key is %f\n", GET_KEY(fh->root));
	if(GET_KEY(target) < GET_KEY(fh->root)){
		fh->root = target;
	}

	return FHEAP_OK;
error:
	return FHEAP_ERR;
}

node *fheap_find_min(fheap *fh)
{
	check(fh != NULL, "Recieved NULL pointer.");

	return fh->root;
error:
	return NULL;
}

/* print all nodes linked to a specific node */
void node_print(node *n)
{

	node *next = n;
	do
	{
		printf("(%p,%d,%f,parent == %p) -> ",next,next->v.v,GET_KEY(next),next->parent);
		next = next->right;
	} while(next != n && next != NULL);

	next = n;
	do
	{
		
		if(next->child){
			printf("\ntree rooted at (%p,%d,%f, parent == %p)\n",next,next->v.v,GET_KEY(next),next->parent);
			node_print(next->child);
		}
		next = next->right;
	} while(next != n && next != NULL);

}

void fheap_print(fheap *fh)
{
	printf("PRINTING FHEAP\n");
	if(fh->root == NULL) return;

	/* call recursively on all nodes in the rootlist */		

	node_print(fh->root);
	printf("\n");
}
