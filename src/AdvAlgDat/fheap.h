#ifndef _fheap_h
#define _fheap_h

#ifndef FHEAP_OK
#define FHEAP_OK (0)
#endif

#ifndef FHEAP_ERR
#define FHEAP_ERR (-1)
#endif

#include <AdvAlgDat/Vertex.h>

struct node;

typedef struct node {
	Vertex v;
	int rank;
	struct node *parent;
	struct node *child; //One of the children
	struct node *left; //Children are in a doubly linked circular list
	struct node *right; //Children are in a doubly linked circular list
	unsigned int mark : 1; //bitfield to set mark
} node;

typedef struct fheap {
	node **node_pool; //We allocate a pool of nodes at the start and hand them out one by one.
	int node_index; //which node is the next node to hand out?
	int node_pool_size;
	node **melding_list; //List for melding heaps. Index correspond to the rank
	node *root;
	int node_count;
} fheap;

/* FHeap Prototypes */
fheap *fheap_create(int n);

void fheap_destroy(fheap *fh);

node *fheap_insert(fheap *fh, Vertex v);

node *fheap_extract_min(fheap *fh);

int fheap_decrease_key(fheap *fh, node *target, float new_key);

node *fheap_find_min(fheap *fh);

void fheap_print(fheap *fh);

/* macros */
#define IS_ROOT(n) (n->parent == NULL)
#define GET_KEY(n) (n->v.k)
#define SET_KEY(n) n->v.k

#endif
