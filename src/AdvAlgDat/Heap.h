#ifndef _heap_h_
#define _heap_h_

#include <AdvAlgDat/Vertex.h>

typedef struct Heap
{
  Vertex *A;
  int *Dict;
  int n;
} Heap;
  
Heap *MakeHeap(Vertex *V, int n, int N);

Vertex SwopHeap(Heap *H, int v, float k);

void DestroyHeap(Heap *h);

Vertex FindMin(Heap *H);

void Insert(Heap *H, int v, float k);

Vertex DeleteMin(Heap *H);

Vertex DecreaseKey(Heap *H, int i, float k);


#endif
