#include <AdvAlgDat/Heap.h>
#include <stdlib.h>

Heap *MakeHeap(Vertex *V, int n, int N)
{
  //printf("\nStarted new makeheap\n");
  Heap *H = malloc(sizeof(*H));
  if (n > 0)
    H->n = 1;
  else
    H->n = 0;

  H->A = V;
  H->Dict = malloc(N * sizeof(int));
  int i;
  for (i = 0; i < n; i++)
    {
      H->Dict[H->A[i].v] = i; // We need this extra memory in order to keep track of the vertices' indices in the queue
      //printf("Set index %d to %d in Dict\n", H->A[i].v, i);
    }
  
  if (n > 1)
    {
      do
	{
	  Insert(H, V[H->n].v, V[H->n].k);
	}
      while (H->n < n);
    }

  
  return H;
}

void DestroyHeap(Heap *h)
{
	if(h)
	  {
	    if (h->Dict) free(h->Dict);
	    free(h);
	  }
}

Vertex SwopHeap(Heap *H, int v, float k)
{
  int i,j;
  Vertex temp,temp1,out;
  if (k <= H->A[0].k)
    {
      out.k = k;
      out.v = v;
      return out;
    }
  else
    {
      i = 1;
      H->A[H->n].k = k;
      H->A[H->n].v = v;
      H->Dict[v] = H->n;
      out = H->A[0];
      for( j = i+i; j <= H->n; j = i+i)
	{
	  temp = H->A[j-1];
	  temp1 = H->A[j];
	  if (temp1.k < temp.k)
	    {
	      temp = temp1;
	      j = j+1;
	    }
	  if (temp.k < k)
	    {
	      H->A[i-1] = temp;
	      H->Dict[temp.v] = i-1;
	      i = j;
	    }
	  else
	    {
	      break;
	    }
	}
      H->A[i-1].k = k;
      H->A[i-1].v = v;
      H->Dict[v] = i-1;
    }
  return out;
}

Vertex DecreaseKey(Heap *H, int v, float k)
{
  Vertex ret;
  int i = H->Dict[v] + 1; // We assume that Dict contains the indices in the queue for each key v
  if (k >= H->A[i-1].k) return ret;
  else
    {
      //printf("DecreaseKey called with argument %f\n", k);
      H->A[i-1].k = k;
      //printf("DecreaseKey subtracted %f from %f\n", k, H->A[i-1]);
      int j;
      while(i>1)
	{
	  j = i/2;
	  if (k < H->A[j-1].k)
	    {
	      H->A[i-1] = H->A[j-1];
	      H->Dict[H->A[i-1].v] = i-1;
	      i = j;
	    }
	  else
	    {
	      break;
	    }
	}
      H->A[i-1].k = k;
      H->A[i-1].v = v;
      H->Dict[v] = i-1;
    }
  return H->A[i-1];
}

Vertex FindMin(Heap *H)
{
  return H->A[0];
}

void Insert(Heap *H, int v, float k)
{
  int i,j;
  i = ++(H->n);
  while(i>1)
    {
      j = i/2;
      if (k < H->A[j-1].k)
	{
	  H->A[i-1] = H->A[j-1];
	  H->Dict[H->A[i-1].v] = i-1;
	  i = j;
	}
      else
	{
	  break;
	}
    }
  H->A[i-1].k = k;
  H->A[i-1].v = v;
  H->Dict[v] = i-1;
}

Vertex DeleteMin(Heap *H)
{
  H->n = H->n -1;
  return SwopHeap(H,H->A[H->n].v,H->A[H->n].k);
}

